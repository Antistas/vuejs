import Vue from 'vue';
import App from './App.vue';
import VueRouter from "vue-router";
import vuetify from './plugins/vuetify';
import Trends from "@/components/Trends";
import Movie from "@/components/Movie";
import Search from "@/components/Search";
import store from './store';
import Person from '@/components/Person';
import moment from 'moment'
Vue.prototype.moment = moment;


Vue.use(VueRouter);
Vue.config.productionTip = false;


const routes = [
  { path: '/', name: 'trends', component: Trends },
  { path: '/search', name: 'news', component: Search },
  { path: '/movie/:id/', name: 'movie', component: Movie },
  { path: '/tv/:id/', name: 'tv', component: Movie },
  { path: '/person/:id', name: 'person', component: Person}
];


const router = new VueRouter({
  routes // сокращённая запись для `routes: routes`
});


new Vue({
  render: h => h(App),
  vuetify,
  router: router,
  store
}).$mount('#app');