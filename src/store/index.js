import Vue from 'vue';
import Vuex from "vuex";
import Axios from "axios";
import menu from '@/menu';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        genresTv: [],
        genresMovie: [],
        apikey: '6eab6bcb0d65708bdd4ed81a7878d39f',
        menu: menu,
        language: [
            '&language=ru-Ru',
            '&language=en-En'
        ],
        currentLanguage: 1
    },
    actions: {
        loadGenres(context) {
            context.commit('loadGenres')
        }
    },
    mutations: {
        loadGenres () {
            Axios
                .get('https://api.themoviedb.org/3/genre/movie/list?api_key=' + this.state.apikey + this.state.language[this.state.currentLanguage])
                .then((response) => {
                    this.state.genresMovie = response.data.genres
                });

            Axios
                .get('https://api.themoviedb.org/3/genre/tv/list?api_key=' + this.state.apikey + this.state.language[this.state.currentLanguage])
                .then((response) => {
                    this.state.genresTv = response.data.genres
                })
        }
    }
});