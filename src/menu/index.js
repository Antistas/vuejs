const menu = [
    { path: '/', text: 'Trends' },
    { path: '/search', text: 'Search' },
];

export default menu;